package Run;

import Base.ShapeBase;
import Shape.Circle;
import Shape.Rectangel;
import Shape.Triangle;

public class Run {
    private static void displayArea(ShapeBase base){
        System.out.println("Display the area for shape " + base.getName() + base.getColor()  + base.getWidth() + "height = " + base.getHeight() + "area is " + base.calculateArea());
    }
    public static void main(String[] args) {
        ShapeBase[] shapes = new ShapeBase[9];
        shapes[0] = new Rectangel("rect", "blue",10,20);
        shapes[1] = new Rectangel("rect", "red",10,20);
        shapes[2] = new Rectangel("rect", "yellow",10,20);
        shapes[3] = new Circle("Circle", "blue",10,20);
        shapes[4] = new Circle("Circle", "red",10,20);
        shapes[5] = new Circle("Circle", "green",10,20);
        shapes[6] = new Triangle("Triangle", "purple",10,20);
        shapes[7] = new Triangle("Triangle", "orange",10,20);
        shapes[8] = new Triangle("Triangle", "grey",10,20);

        for (int i=0; i<shapes.length; i++){
            displayArea(shapes[i]);
        }
    }
}
