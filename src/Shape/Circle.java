package Shape;

import Base.ShapeBase;

public class Circle extends ShapeBase {
    public Circle(String name, String color, int width, int height) {
        super(name, color, width, height);
    }
    public int calculateArea(){
        return (int) (width/2*3.1415);
    }
}
