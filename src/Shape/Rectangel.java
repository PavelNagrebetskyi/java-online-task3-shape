package Shape;

import Base.ShapeBase;

public class Rectangel extends ShapeBase {

    public Rectangel(String name, String color, int width, int height) {
        super(name, color, width, height);
    }

    public int calculateArea(){
        return width*height;
    }
}
