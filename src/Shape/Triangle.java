package Shape;

import Base.ShapeBase;

public class Triangle extends ShapeBase {
    public Triangle(String name, String color, int width, int height) {
        super(name, color, width, height);
    }
    public int calculateArea(){
        return width * height / 2;
    }
}
