package Base;

    public class ShapeBase implements ShapeIntarface {

        protected String name;
        protected String color;
        protected int width, height;

        public ShapeBase(String name, String color, int width, int height) {
            this.name = name;
            this.color = color;
            this.width = width;
            this.height = height;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        @Override
        public int calculateArea() {
            return 0;
        }
    }
